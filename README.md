Endorsements
````````````

Endorsements are the 'weight' in which Zing0 search results will be weighted.
An endorsement must:

* maximize Endorser privacy
* enable the receipt of ZEC
* provide a fulfillment mechanism
* provide a retraction capability

Proof Of Concept Implementation
```````````````````````````````

An `Endorsement` is a noun.  A thing. To `Endorse` is the infinitive.  The act of Endorsing creates an Endorsement.   A url is said to be "endorsed", if the
`endorse` action was taken upon it.

ZingO will, when displaying results, arrange results such that URLs that have more active endorsements will be closer to the top of the list.
This means that endorsements are primarily a measure of how much zec Zing0 users have 'bet' that the endorsed URL has value.
The secondary purpose to endorsements is for users to pay other users for Zing0 curation, by 'fulfilling' endorsements. Sending zec to the authors of the endorsements, and optionally communicating a request for further interaction/information.
In order to endorse a URL (i.e. create an endorsement), a zingo user must, by clicking the "endorse" button:

    A) Create a transparent address, into which the minimum endorsement amount (MEA) will be deposited from a shielded address owned by the user.
    B) Create an empty shielded address, which will be used to verify payment, and allow for further communication between endorser and fulfiller.
    C) Send both addresses, along with the URL to be endorsed, to the extensionzcashzproxy.

Thus, an endorsement contains a transparent address, a shielded address, and the target endorsement URL, that are linked by the endorsement creation process. The transparent address contains the MEA of ZEC, sent by the endorser upon creation of the endorsement.

To fulfill an endorsement, the fulfiller sends an amount of zec equal to the size of the endorsement to the transparent address. (in this iteration, it will always be the MEA) This marks the endorsement as fullfilled, so that it will no longer contribute its 'weight' to Zing0, and allows the endorser to spend the zec in the address. Optionally, the fulfiller may also send a transaction to the shielded address that contains (possibly through use of the memo field) proof that they sent the ZEC to the transparent address, (Perhaps some sort of fingerprint?) {is there a mechanism for sending from a private address that doesn't reveal that address, but is provable later, with a certain viewing key?} generally with the intent of establishing private communication. For the
POC, retraction is simply accomplished via self-fullfillment. (Alternatively, 
simply transfering the ZEC out, as it's a transparent address?)

 Endorsements are arranged in a vector, where order is publicly commited to by XXX 
