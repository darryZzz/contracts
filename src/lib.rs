pub enum EndorsementError {}
pub enum Confirmation {}
pub struct Endorsement {
    endorser_ivk: [u8; 1024],                 // I just picked a plausible size.
    endorser_diversified_address: [u8; 1024], // I just picked a plausible size.
}
impl Endorsement {
    pub fn new(endorser_ivk: [u8; 1024], endorser_diversified_address: [u8; 1024]) -> Endorsement {
        Endorsement {
            endorser_ivk,
            endorser_diversified_address,
        }
    }
    pub fn fulfill_endorsement(
        &self,
        appreciator_spend_key: [u8; 1024],
    ) -> Result<Confirmation, EndorsementError> {
        unimplemented!()
    }
}
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
